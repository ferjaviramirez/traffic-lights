import { LitElement, css, html } from 'lit';
import {Router} from '@vaadin/router';
import {SpaService} from '../services/spa.service';

export class HomeComponent extends LitElement {
    static properties = {
        title: { type: String },
        validacion: {type: String},
        inputValue:{type: String},
        users: {type: Array},
        userName: {type: String},
        dataPlayer:{type: String}
    };

    constructor() {
        super();
        this.title = 'Login Player';
        this.users = [
            {user:'fernando', maxScore: 15},
            {user:'javier', maxScore: 10},
            {user:'ramirez', maxScore: 5}
        ];
    }


    connectedCallback() {
        super.connectedCallback();
        this.addEventListener('keyup', (event) => {
            if (event.key === 'Enter') {
                event.preventDefault();
                this.login();
            }
        });
    }

    static styles = css`

    .login-content{
        max-width:200px;
        position: absolute;
        top: 50%;
        left: 50%;
       transform: translate(-50%, -50%);
    }
    small{
        color: #008CBA; 
    }

    .login-content button{
        margin-top: 1rem;
    }

    .login-content img{
        height:50px;
    }

    .login-content input{
        margin-bottom: 1rem;
    }

     .button {
       background-color: black; 
       border: 1px solid #008CBA;
       color: white;
       padding: 0px 32px;
       text-align: center;
       text-decoration: none;
       display: inline-block;
       font-size: 16px;
       margin: 4px 2px;
       transition-duration: 0.4s;
       cursor: pointer;
       border-radius: 3px;
   }

   .button:hover {
       background-color: #008CBA;
       color: white;
   }

   .errorMessage{
       height: 3rem;
   }
   

   .d-none{
       visibility: hidden;
   }

   .fade-in {
        animation: fadeIn 2s;
        opacity: 1;
    }

    @keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }

 `;


    render() {
        return html`
            <div class="login-content fade-in">
            <img style="-webkit-user-select: none;margin: auto;/* background-color: hsl(0, 0%, 90%); */transition: background-color 300ms;" src="https://img.icons8.com/color/344/mouse.png">
            <h2>${this.title}</h2>
                <input type="text" id="nickname" name="nickname">
                <div class="errorMessage" id="errorMessage"></div>
                <button id="buttonLogin" @click="${this.login}" class="button">Join</button>
            </div>
        `;
    }


    login() {
        let autenticated = false;
        let errorMesssage = this.renderRoot.querySelector("#errorMessage");
        this.inputValue = this.renderRoot.querySelector("#nickname").value;
        this.users.forEach((element,index,array)=>{
            if(element.user.includes(this.inputValue)){
                this.dataPlayer = element;
                autenticated = true;
            }
        });
        if(autenticated){
            this.userName = this.inputValue;
            SpaService.setUser( this.dataPlayer);
            Router.go('/game');
        }else{
            errorMesssage.innerHTML = '<small class="fade-in">Wrong Username please try again.</small>';
        }
        this.renderRoot.querySelector("#nickname").value ="";
    }

}
customElements.define('home-page', HomeComponent);
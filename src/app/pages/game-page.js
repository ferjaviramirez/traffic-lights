import { LitElement, css, html } from 'lit';
import { SpaService } from '../services/spa.service';
import {Router} from '@vaadin/router';

export class GameComponent extends LitElement {
    static properties = {
        nameUser: { type: String },
        count: { type: Number },
        highScore: { type: Number },
        userPoints: { type: Number }
    };
    static styles = css`
    span{
        color: #008CBA;
    }

    small{
        color:red;
    }

    .fade-in {
        animation: fadeIn 2s;
        opacity: 1;
    }

    .d-none{
        display:none !important;
    }

    .home-content{
        max-width:200px;
        position: absolute;
        top: 40%;
        left: 50%;
       transform: translate(-50%, -50%);
    }

    .button {
       background-color: black; 
       border: 1px solid #008CBA;
       color: white;
       padding: 3px 32px;
       text-align: center;
       text-decoration: none;
       display: inline-block;
       font-size: 16px;
       margin: 4px 2px;
       transition-duration: 0.4s;
       cursor: pointer;
       border-radius: 3px;
   }

   .button:hover {
       background-color: #008CBA;
       color: white;
   }

   .traffic-light{
       width: 100px;
       height: 100px;
       color: green;
   }

   .red-color{
    color: red;
   }

   .traffic-buttons{
       display:flex;
       flex-direction: row;
   }


   .disabled{
            cursor: not-allowed;
            pointer-events: none;
            color: #c0c0c0;
            background-color: #ffffff;
        }

    @keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }


  `;

    constructor() {
        super();
        this.userName = SpaService.getData();
        this.highScore = 1000;
        this.userPoints = 10000;
        setTimeout(()=>{
            if(!this.userName){
                Router.go('/home');
            }
        },200)
    }


    connectedCallback() {
        super.connectedCallback();
        this.count = 0;
        this.highScore = 15;
    }

    firstUpdated() {
        setInterval(() => {
            this.changeLightsColor();
        }, this.userName.maxScore * 1000);
    }



    render() {
        const { count } = this;
        return html`
        <navbar-component .userNavName=${this.userName.user}></navbar-component>
        <div class="container fade-in home-content">
            <div class="container">
                <h2>Welcome to the <span>Squid</span> game.</h2>
                <p>High Score : ${this.userName.maxScore}</p>
                <svg class="traffic-light " id="trafficLight" xmlns="http:www.w3.org/2000/svg" fill="currentColor"
                    class="bi bi-stoplights" viewBox="0 0 16 16">
                    <path
                        d="M8 5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm0 4a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm1.5 2.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                    <path
                        d="M4 2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2h2c-.167.5-.8 1.6-2 2v2h2c-.167.5-.8 1.6-2 2v2h2c-.167.5-.8 1.6-2 2v1a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2v-1c-1.2-.4-1.833-1.5-2-2h2V8c-1.2-.4-1.833-1.5-2-2h2V4c-1.2-.4-1.833-1.5-2-2h2zm2-1a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H6z" />
                </svg>
                <p>Score: ${count}</p>
                <div class="traffic-buttons">
                    <button id="leftButton" @click=${() => this.setCount(count + 1, this.userName.maxScore + 1)}
                        class="button">Left</button>
                    <button id="rightButton" @click=${() => this.setCount(count + 1, this.userName.maxScore + 1)}
                        class="button">Right</button>
                </div>
                <div class="gameMessage" id="gameMessage"></div>
                <button @click="${this.returnButton}" id="returnButton" class="button d-none">Login</button> 
            </div>
        </div>
        `;
    }


    changeLightsColor() {
        let trafficLight = this.shadowRoot.getElementById('trafficLight');
        trafficLight.classList.add('red-color');
        setTimeout(function () {
            trafficLight.classList.remove('red-color')
        }, 3000)

    }

    setCount(count, higScore) {
        let trafficLight = this.shadowRoot.getElementById('trafficLight');
        let rightButton = this.shadowRoot.getElementById('rightButton');
        let leftButton = this.shadowRoot.getElementById('leftButton');
        let returnButton = this.shadowRoot.getElementById('returnButton');
        let gameMessage = this.renderRoot.querySelector("#gameMessage");
        if (trafficLight.classList.contains('red-color')) {
            rightButton.classList.add('disabled');
            leftButton.classList.add('disabled');
            returnButton.classList.remove('d-none');
            gameMessage.innerHTML = `
            <small class="fade-in">Game Over <br> please return to Login</small>
            <div id="countdown"></div>
            `;
            return this.count = 0;
        }
        this.count = count;
        if (this.count > this.userName.maxScore) {
            this.userName.maxScore = higScore;
        }
    }

    returnButton(){
        console.log('funciona');
        Router.go('/home');
    }


}
customElements.define('game-page', GameComponent);
import './main.css';

//pages 
import './app/pages/home-page';
import './app/pages/game-page';

//components
import './app/components/navbar-component';

//router
import './router';
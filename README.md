# Test BBVA Red, Green game.


## Descripcion
Este es una aplicacion de juego en Lit-element que se basa en hacer la mayor cantidad de clicks mientras el semaforo este de color verde.
Podeis ver la app online desde este enlace: https://traffic-lights-bbva.netlify.app/home


## Instalacion
1- Clonar desde el respositorio la aplicacion con el siguiente enlance:  https://gitlab.com/ferjaviramirez/traffic-lights
2- Instalar todas las dependencias del proyecto usando el siguiente comando: npm install 
3- Levantar el servidor en local con el siguiente comando npm run start


## Como usar
En el login se puede ingresar a la app con los siguientes nombres de usuarios: fernando , javier , ramirez . luego se ingresa al game-page donde inmediatamente se 
podra jugar haciendo click a los botones mientras el semaforo este de color verde, si cambia a color rojo y se hace click durante ese intervalo inemdiatamente 
se termina el juego y hay que rederigirse al login para volver a jugar.

 ## Licencia
ferjaviramirez@gmail.com